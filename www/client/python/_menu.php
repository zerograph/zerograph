
<aside>
<h1 class="menu">Python Client</h1>
<ul class="menu">
    <li><a href=".">Overview</li>
    <li><a href="classes">Classes</a></li>
    <ul class="menu">
        <li><a href="Graph">Graph</a></li>
        <li><a href="Node">Node</a></li>
        <li><a href="Rel">Rel/Rev</a></li>
        <li><a href="Path">Path</a></li>
        <li><a href="Batch">Batch</a></li>
    </ul>
</ul>
</aside>

