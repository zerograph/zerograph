<!doctype html>
<html>
<head>
<?php include '../_head.php' ?>
<title>Cypher - Server - Zerograph</title>
</head>
<body>

<?php include '../_header.php' ?>

<?php include '_menu.php' ?>

<main>

<h1>Cypher</h1>
<p>The <strong>Cypher</strong> resource provides an endpoint against which
<a href="http://www.neo4j.org/learn/cypher">Cypher</a> queries can be executed.
</p>


<h2>Methods</h2>

<h3>EXECUTE Cypher {"query": &hellip;, "params": &hellip;}</h3>
<p>Execute a Cypher query with a set of named parameters.
</p>

</main>

<?php include '_footer.php' ?>
</body>
</html>
