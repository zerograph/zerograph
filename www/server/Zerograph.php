<!doctype html>
<html>
<head>
<?php include '../_head.php' ?>
<title>Zerograph - Server - Zerograph</title>
</head>
<body>

<?php include '../_header.php' ?>

<?php include '_menu.php' ?>

<main>

<h1>Zerograph</h1>
<p>The <strong>Zerograph</strong> resource represents the entire Zerograph
server application and can be used to retrieve details about the services
available and system variables.
</p>

<h2>Methods</h2>

<h3>GET Zerograph</h3>
<p>Fetch details of the Zerograph server application.
</p>

</main>

<?php include '_footer.php' ?>
</body>
</html>
