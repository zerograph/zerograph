<!doctype html>
<html>
<head>
<?php include '../_head.php' ?>
<title>Resources - Server - Zerograph</title>
</head>
<body>

<?php include '../_header.php' ?>

<?php include '_menu.php' ?>

<main>

<h1>Resources</h1>

<ul>
    <li><a href="Cypher">Cypher</a></li>
    <li><a href="Graph">Graph</a></li>
    <li><a href="Node">Node</a></li>
    <li><a href="NodeSet">NodeSet</a></li>
    <li><a href="Rel">Rel</a></li>
    <li><a href="RelSet">RelSet</a></li>
    <li><a href="Zerograph">Zerograph</a></li>
</ul>

</main>

<?php include '_footer.php' ?>
</body>
</html>
