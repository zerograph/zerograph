#!/usr/bin/env bash

sudo apt-get install -y build-essentials

cd src/client/python
sudo pip install -r requirements.txt

